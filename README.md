# Switch from Rocky Linux to ClearOS

This script is designed to automatically switch a Rocky Linux instance to ClearOS
by removing any Rocky-specific packages or replacing them with the ClearOS
equivalent.

## Supported versions

This script currently supports switching Rocky Linux 8.

## Before you switch

**IMPORTANT:** this script is a work-in-progress and is not designed to handle
all possible configurations. Please ensure you have a **complete backup** of the
system _before_ you start this process in the event the script is unable to
convert the system successfully.

## Usage

1. Login to your Rocky Linux 8 instance as a user who has `sudo` privileges.
1. Either clone this repository or download the [`rocky2clear.sh`][3] script.
1. Run `sudo bash rocky2clear.sh` to switch your Rocky instance to ClearOS.

### Usage options

* `-r` Reinstalls all Rocky RPMs with ClearOS RPMs

   If a system is swiched to ClearOS and there is no newer ClearOS version
   of a package already installed then the Rocky version remains.
   This option proceeds to reinstall any Rocky RPM with an identical version from
   ClearOS. This is not necessary for support and has no impact to a systems functionality
   but is offered so a user can remove Rocky GPG keys from the truststore.
   A list of all non-ClearOS RPMs will be displayed after the reinstall process.

* `-V` Verify RPM information before and after the switch

  This option creates four output files in `/var/tmp/`:

  * `${hostname}-rpms-list-[before|after].log`: a sorted list of installed
    packages `before` and `after` the switch to ClearOS.
  * `${hostname}-rpms-verified-[before|after].log`: the RPM verification results
     for all installed packages `before` and `after` the switch to ClearOS.

## Testing

See [`TESTING.md`](./TESTING.md) for instructions on the available tests and
how to run them.

## Known issues

1. There is a [reported issue with the upstream OpenJDK][9] package resetting the
   `alternatives` configuration during a `dnf reinstall` transaction.

   We recommend recording the output of `alternatives --list` prior to running
   `rocky2clear.sh` and reviewing the same output after switching. If you experience
   an issue with a package other than OpenJDK, please [open an issue][6]

## Limitations

1. The script currently needs to be able communicate with the Rocky and ClearOS
   yum repositories either directly or via a proxy.
1. Compatibility with packages installed from third-party repositories is
   expected but not guaranteed. Some software doesn't like the existence of an
   `/etc/clearos-release` file, for example.
1. The script only enables the base repositories required to enable switching
   to ClearOS. Users may need to enable additional repositories to obtain
   updates for packages already installed (see [issue #1][4]).

## Debugging

Run `sudo bash -x rocky2clear.sh` to switch your Rocky instance to ClearOS
in debug mode. This will print a trace of commands and their arguments or
associated word lists after they are expanded but before they are executed.

## Get involved

We welcome contributions! See our [contribution guidelines][5].

## Support

* Open a [GitLab issue][6] for non-security related bug reports, questions, or
  requests for enhancements.
* To report a security issue or vulnerability, please follow the
  [reporting security vulnerabilities][7] instructions.

## Resources

For more information on ClearOS, please visit [clearos.com][8].

## License

Copyright (c) 2020, 2021 Oracle and/or its affiliates.

Licensed under the Universal Permissive License v 1.0 as shown at
<https://oss.oracle.com/licenses/upl/>

[1]: https://blogs.oracle.com/linux/cve-2020-10713-grub2-boothole
[2]: https://wiki.centos.org/AdditionalResources/Repositories/CentOSPlus
[3]: https://raw.githubusercontent.com/oracle/centos2ol/main/centos2ol.sh
[4]: https://github.com/oracle/centos2ol/issues/1
[5]: ./CONTRIBUTING.md
[6]: https://github.com/oracle/centos2ol/issues
[7]: ./SECURITY.md
[8]: https://www.clearos.com
